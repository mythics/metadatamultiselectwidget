﻿# MetadataMultiSelectWidget

## Component Information
* Created by: Mythics
* Author: Jonathan Hult
* Last Updated: build_8_20140305
* License: MIT

## Overview
This WebCenter Content component transforms specified multiselect option lists to use the bootstrapTransfer multiselect widget. The component can be globally enabled/disabled system wide via the preference prompt MetadataMultiSelectWidget_ComponentEnabled. This functionality can be enabled/disabled for individual metadata fields by the use of the configuration setting fieldName:useMultiSelectWidget. 
	
* Dynamichtml includes:
	- std_js_bootstrap_vars: Core - override to load bootstrapTransfer JS/CSS and jQuery
	- std_edit_entry: Core - override to hide default field if isUseMultiSelectWidget is enabled for this field
	- std_option_list_entry: Core - override for each field to use bootstrapTransfer multiselect widget if isUseMultiSelectWidget is enabled for this field 
	
* Preference Prompts:
	- MetadataMultiSelectWidget_ComponentEnabled: boolean determining whether component functionality is enabled

* Configuration settings and variables
	- fieldName:isUseMultiSelectWidget: if set to 1 and this field is a multiselect option list, it will use the bootstrapTransfer multiselect widget
	- isMultiSelectWidgetEnabled: if set to 1, the bootstrapTransfer JS/CSS and jQuery will be loaded on this page

* Published Static Files:
	- resources/metadatamultiselectwidget/bootstraptransfer/license.txt
	- resources/metadatamultiselectwidget/bootstraptransfer/css/bootstrap-transfer.css
	- resources/metadatamultiselectwidget/bootstraptransfer/img/bootstrap-transfer-default-bg.gif
	- resources/metadatamultiselectwidget/bootstraptransfer/img/bootstrap-transfer-nav-bg.gif
	- resources/metadatamultiselectwidget/bootstraptransfer/img/bootstrap-transfer-selector-add.gif
	- resources/metadatamultiselectwidget/bootstraptransfer/img/bootstrap-transfer-selector-addall.gif
	- resources/metadatamultiselectwidget/bootstraptransfer/img/bootstrap-transfer-selector-remove.gif
	- resources/metadatamultiselectwidget/bootstraptransfer/img/bootstrap-transfer-selector-removeall.gif
	- resources/metadatamultiselectwidget/bootstraptransfer/img/glyphicons-halflings.png
	- resources/metadatamultiselectwidget/bootstraptransfer/js/bootstrap-transfer.js
	
## Compatibility
This component was built upon and tested on the version listed below, this is the only version it is known to work on, but it may work on older/newer versions.

* 10.1.3.5.1 (111229) (Build:7.2.4.105)
* 11.1.1.8.0PSU-2013-08-27 18:40:56Z-r109125 (Build:7.3.5.185)

## Changelog
* build_8_20140305
	- Added code for checkin revision 2+ (dpAction = CheckinSel)
	- Fixed display issue where WebCenter Content requires key to have spaces after commas (to properly display values)
* build_7_20131211
	- Allow metadata updates to populate properly
* build_6_20131120
	- Fixed issue with spaces in key
* build_5_20131114
	- Removed choose all option
	- Fixed issue with populating list from view
* build_4_20131113
	- Fixed case sensitive issue for loading bootstrapTransfer JS/CSS
	- Fixed publish srcPath
	- Changed remove button color from gray to blue
* build_3_20131030
	- Lowered publish loadOrder and changed publish class
* build_2_20131016
	- Added isMultiSelectWidgetEnabled variable
	- Enabled option lists which pull from a schema view
* build_1_20131010
	- Initial component release